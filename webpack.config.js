const path = require('path');
const {VueLoaderPlugin} = require('vue-loader');

module.exports = {
  mode: 'production',
  entry: {
    main: './src/main.js',
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: 'babel-loader',
      },
      {
        test: /\.vue$/,
        exclude: /node_modules/,
        loader: 'vue-loader',
      },
      {
        test: /(\.css)$/,
        use: [
          {
            loader: 'css-loader',
            options: {
              esModule: false,
            },
          },
        ],
      },
    ]
  },
  plugins: [
    new VueLoaderPlugin(),
  ],
  output: {
    filename: '[name].js',
    path: path.resolve(__dirname, 'dist'),
    clean: true,
    library: {
      type: 'commonjs2',
      export: 'default'
    },
  },
};